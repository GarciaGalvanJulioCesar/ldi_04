import java.util.Scanner;
 
 
public class Binariohexa {
    
    static final String Hexformato = "0123456789ABCDEF";
    static Scanner inputScanner = new Scanner(System.in);
     
    public static void main(String... args)
    {
         //Pide el ingreso de un numero en binario  
        
        System.out.println("Ingrese un valor en binario para convertir a Hex :");
        System.out.println(deHex(aBin(inputScanner.nextLine())));
        
        //Pide el ingreso de un numero en exadecimal
        
        System.out.println("Ingrese un valor en Hex para convertir a binario:");
        System.out.println(deBin(aHex(inputScanner.nextLine())));
    }
     
    static String deHex(long value)
    {
        int align = 0;
        StringBuilder output = new StringBuilder(32);
        while(value > 0 || align % 2 != 0){
            output.append(Hexformato.charAt((int)(value & 0x0F)));
            value >>= 4;
            align++;
            
        }
        
        return output.reverse().toString();
    }
     
    static long aHex(String hex)
    {
        long value = 0;
        for(int i = 0; i < hex.length(); i++){
            value <<= 4;
            value |= Hexformato.indexOf(hex.charAt(i));
        }
        return value;
    }
     
    static String deBin(long value)
    {
        int align = 0;
        StringBuilder output = new StringBuilder(32);
        while(value > 0 || align % 8 != 0){
            output.append((((int)value & 1) == 1)?'1':'0');
            value >>= 1;
            align++;
        }
        return output.reverse().toString();
    }
     
    static long aBin(String bin)
    {
        long value = 0;
        for(int i = 0; i < bin.length(); i++){
            value <<= 1;
            if(bin.charAt(i) == '1') value |= 1;
        }
        return value;
    }
     
}